package de.kilobyte22.plugin.kibi.misccmds;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class HTTP {
    public static String get(URL url) throws IOException {
        return new Scanner( url.openStream() ).useDelimiter( "\\Z" ).next();
    }
}
