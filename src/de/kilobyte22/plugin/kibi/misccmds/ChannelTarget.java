package de.kilobyte22.plugin.kibi.misccmds;

import de.kilobyte22.app.kibibyte.misc.IMessageTarget;
import org.pircbotx.Channel;

public class ChannelTarget implements IMessageTarget {

    private final Channel channel;
    private int usecount = 0;

    public ChannelTarget(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void print(String message) {
        if (usecount > 2) throw new RuntimeException();
        channel.getBot().sendMessage(channel, message);
    }

    @Override
    public String getName() {
        return channel.getName();
    }
}
