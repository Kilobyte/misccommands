package de.kilobyte22.plugin.kibi.misccmds;

public class HTMLResponse {
    private final String resBody;

    public HTMLResponse(String resBody) {

        this.resBody = resBody;
    }

    public String getResBody() {
        return resBody;
    }
}
