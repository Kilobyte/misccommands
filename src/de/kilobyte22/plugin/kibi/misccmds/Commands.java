package de.kilobyte22.plugin.kibi.misccmds;

import de.kilobyte22.app.kibibyte.command.Command;
import de.kilobyte22.app.kibibyte.command.CommandHandler;
import de.kilobyte22.app.kibibyte.exceptions.CommandException;
import de.kilobyte22.app.kibibyte.exceptions.UsageException;
import de.kilobyte22.app.kibibyte.misc.TimeUtils;
import de.kilobyte22.lib.configuration.ConfigList;
import de.kilobyte22.lib.configuration.ConfigMap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Commands extends CommandHandler {
    //@Command(name = "google", usage = "[-p/--page <page>] <request> [-c/--channel]", help = "looks for a certain term on the Popular Search Engine Google")
    public void google() {

        String term = args.getOrError(1);
        try {
            HTMLRequest request = new HTMLRequest(new URL("http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=" + term + "&safe=off"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Command(name = "raw", usage = "[-n/--now] <raw line>", help = "Sends a raw line", permission = "misc.admin.raw")
    public void raw() {
        if (args.getNamedParam("now", 'n') == null)
            MiscCommands.botAccess.bot.sendRawLine(args.merge(1, 0));
        else
            MiscCommands.botAccess.bot.sendRawLineNow(args.merge(1, 0));
    }

    @Command(name = "tell", usage = "<target> <message>", help = "Schedules a message for someone", permission = "misc.tell")
    public void tell() {
        String target = args.getOrError(1).toLowerCase();

        if (target.equalsIgnoreCase(sender.getNick()) || target.equalsIgnoreCase(sender.getBot().getNick())) {
            print("seriously?!");
            return;
        }

        String message = args.merge(2, 0);
        if (message.length() < 5)
            throw new CommandException("Message too short");
        ConfigMap list = MiscCommands.tellStorage.getMap("messages");
        ConfigList user = list.getList(target);
        ConfigMap msg = new ConfigMap();
        msg.put("sender", sender.getNick());
        msg.put("message", message);
        user.add(msg);
        MiscCommands.tellStorage.save();
        print("I'll pass that along :)");
    }

    @Command(name = "remember", aliases = {"r"}, usage = "[-i/--interpreter interpreter] <name> <text>", help = "Saves a factoid to be recalled later", permission = "factoid.remember", extendedCharArgs = "it", extendedLongArgs = {"interpreter", "type"})
    public void remember() {
        String nsaccount = MiscCommands.botAccess.nickservSystem.getAccount(sender.getNick());
        if (nsaccount == null)
            nsaccount = "Not Identified";
        String name = args.getOrError(1).toLowerCase();
        String message = args.merge(2, 0);
        String interp = args.getNamedParam("interpreter", 'i');
        if (message.length() < 1)
            throw new UsageException();
        ConfigMap factoid = MiscCommands.factoids.getMap(name);
        if (factoid.getBoolean("locked", false) && (!factoid.getString("lockacc").equalsIgnoreCase(nsaccount)) && (!MiscCommands.botAccess.permissionSystem.hasParsedPermission("factoid.admin.bypasslock", sender, channel))) {
            throw new CommandException("This factoid is locked by the account " + factoid.getString("lockacc"));
        }
        factoid.put("authornick", sender.getNick());
        factoid.put("authoracc", nsaccount);
        factoid.put("editdate", TimeUtils.getCurrentTimeStamp());
        if (interp != null)
            factoid.put("interpreter", interp);
        else
            factoid.remove("interpreter");
        String type = args.getNamedParam("type", 't');
        if (type != null)
            factoid.put("type", type);
        else
            factoid.put("type", "regular");
        factoid.put("data", message);

        MiscCommands.factoids.put(name, factoid);
        MiscCommands.factoids.save();
        print("Done.");
    }

    @Command(name = "factoidinfo", usage = "<factoid>", help = "displays details on a factoid", permission = "factoid.info")
    public void finfo() {
        String name = args.get(1);
        if (!MiscCommands.factoids.containsKey(name))
            throw new CommandException("Unknown factoid");

        ConfigMap factoid = MiscCommands.factoids.getMap(name);
        String lang = factoid.getString("interpreter"), nick = factoid.getString("authornick"), acc = factoid.getString("authoracc"), type = factoid.getString("type", "regular");
        lang = lang == null ? "Plaintext" : lang;

        nick = nick.equalsIgnoreCase(acc) ? nick : nick + "[" + acc + "]";
        String lock = "";
        if (factoid.getBoolean("locked", false))
            lock = String.format("; Locked by %s on %s", factoid.getString("lockacc"), TimeUtils.dateFromTimeStamp(factoid.getLong("lockdate", 0L)).toString());
        if (!type.equalsIgnoreCase("alias")) {
            String action = type.equalsIgnoreCase("action") ? "; Factoid output happens as action" : "";

            print("%s Bytes, Last edited by %s on %s, Language is %s" + lock + action, factoid.getString("data").length() + "", nick, TimeUtils.dateFromTimeStamp(factoid.getLong("editdate", 0L)).toString(), lang);
            print("Raw data is: " + factoid.getString("data"));
        } else {
            print("Alias for " + factoid.getString("data") + lock);
        }

    }

    @Command(name = "forget", aliases = {"f"}, usage = "<name>", help = "removes a factoid", permission = "factoid.forget")
    public void forget() {
        String nsaccount = MiscCommands.botAccess.nickservSystem.getAccount(sender.getNick());
        if (nsaccount == null)
            nsaccount = "Not Identified";
        String name = args.get(1);
        if (!MiscCommands.factoids.containsKey(name))
            throw new CommandException("Unknown factoid");
        ConfigMap factoid = MiscCommands.factoids.getMap(name);
        if (factoid.getBoolean("locked", false) && (!factoid.getString("lockacc").equalsIgnoreCase(nsaccount)) && (!MiscCommands.botAccess.permissionSystem.hasParsedPermission("factoid.admin.bypasslock", sender, channel))) {
            throw new CommandException("This factoid is locked by the account " + factoid.getString("lockacc"));
        }
        MiscCommands.factoids.remove(name);
        MiscCommands.factoids.save();
        print("Done.");
    }

    @Command(name = "flock", usage = "[-a/--as user] <name>", help = "Locks a factoid to prevent it from being edited or removed", permission = "factoid.admin.lock", extendedLongArgs = {"as"}, extendedCharArgs = "a")
    public void lock() {
        String nsaccount = MiscCommands.botAccess.nickservSystem.getAccount(sender.getNick());
        if (nsaccount == null)
            nsaccount = "Not Identified";
        String lockas = args.getNamedParam("as", 'a');
        if (lockas == null)
            lockas = nsaccount;
        String name = args.get(1).toLowerCase();
        if (!MiscCommands.factoids.containsKey(name))
            throw new CommandException("Unknown factoid");
        ConfigMap factoid = MiscCommands.factoids.getMap(name);
        if (factoid.getBoolean("locked", false)) {
            if (factoid.getString("lockacc").equalsIgnoreCase(nsaccount)) {
                throw new CommandException("You already locked this factoid");
            } else if (!MiscCommands.botAccess.permissionSystem.hasParsedPermission("factoid.admin.bypasslock", sender, channel)) {
                throw new CommandException("This factoid is already locked by the account " + factoid.getString("lockacc"));
            }
        }
        factoid.put("locked", true);
        factoid.put("lockacc", lockas);
        factoid.put("lockdate", TimeUtils.getCurrentTimeStamp());
        MiscCommands.factoids.save();
        print("Done.");
    }

    @Command(name = "funlock", usage = "<name>", help = "Removes the lock of a factoid", permission = "factoid.admin.unlock")
    public void unlock() {
        String nsaccount = MiscCommands.botAccess.nickservSystem.getAccount(sender.getNick());
        if (nsaccount == null)
            nsaccount = "Not Identified";
        String lockas = args.getNamedParam("as", 'a');
        if (lockas == null)
            lockas = nsaccount;
        String name = args.get(1).toLowerCase();
        if (!MiscCommands.factoids.containsKey(name))
            throw new CommandException("Unknown factoid");
        ConfigMap factoid = MiscCommands.factoids.getMap(name);
        if (factoid.getBoolean("locked", false)) {
            if (factoid.getString("lockacc").equalsIgnoreCase(nsaccount) || MiscCommands.botAccess.permissionSystem.hasParsedPermission("factoid.admin.bypasslock", sender, channel)) {
                factoid.put("locked", false);
                factoid.remove("lockacc");
                factoid.remove("lockdate");
                MiscCommands.factoids.save();
                print("Done.");
            } else {
                throw new CommandException("This factoid is locked by the account " + factoid.getString("lockacc") + ". You cannot unlock it");
            }
        }
    }

    @Command(name = "seen", usage = "<nick>", help = "Tells you when someone was seen last by the bot", permission = "misc.seen")
    public void seen() {
        String name = args.getOrError(1).toLowerCase();
        if (name.equalsIgnoreCase(sender.getNick()) || name.equalsIgnoreCase(sender.getBot().getNick())) {
            print("seriously?!");
            return;
        }
        if (MiscCommands.seenData.containsKey(name)) {

            ConfigMap user = MiscCommands.seenData.getMap(name);
            print("%s was last seen on %s in %s saying '%s'", name, TimeUtils.dateFromTimeStamp(user.getLong("timestamp", null)) + "", user.getString("channel"), user.getString("message"));
        } else {
            print("I never saw that person");
        }
    }

    @Command(name = "channel", usage = "(join|part|blacklist (add|remove)) <channel> [partmsg]", help = "Manages channels", permission = "core.admin.channel")
    public void channel() {
        String command = args.getOrError(1);
        if (command.equalsIgnoreCase("join")) {
            botAccess.bot.join(args.getOrError(2));
        } else if (command.equalsIgnoreCase("part")) {
            String channel = args.getOrError(2);
            String message = args.merge(3, 0);
            if (message.length() == 0) message = "Part command used by " + sender.getNick();
            else message = "[" + sender.getNick() + "] " + message;
            botAccess.bot.part(channel, message);
        } else if (command.equalsIgnoreCase("blacklist")) {
            boolean autopart = args.getNamedParam("part", 'p') != null;
            command = args.getOrError(2);
            String channel = args.getOrError(3).toLowerCase();
            if (command.equalsIgnoreCase("add")) {
                MiscCommands.config.getList("channelblacklist").add(channel);
                MiscCommands.config.save();
                if (autopart)
                    botAccess.bot.part(channel, "Channel blacklisted by " + sender.getNick());
            } else if (command.equalsIgnoreCase("remove")) {
                MiscCommands.config.getList("channelblacklist").removeObject(channel);
                MiscCommands.config.save();
            } else throw new UsageException();
        } else throw new UsageException();
    }

    @Command(name = "clones", usage = "[-r/--regex] hostmask", help = "Lists all users in a channel matching a certain hostmask", permission = "misc.clones")
    public void clones() {

    }
}
