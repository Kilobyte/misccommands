package de.kilobyte22.plugin.kibi.misccmds;

import de.kilobyte22.app.kibibyte.api.InterpreterRegestry;
import de.kilobyte22.app.kibibyte.misc.IInterpreter;
import de.kilobyte22.app.kibibyte.misc.IMessageTarget;
import de.kilobyte22.app.kibibyte.misc.Match;
import de.kilobyte22.app.kibibyte.misc.Utils;
import de.kilobyte22.lib.configuration.ConfigMap;
import de.kilobyte22.plugin.kibi.misccmds.api.IFactoidManager;
import org.pircbotx.User;

import java.util.LinkedList;
import java.util.List;

public class FactoidManger implements IFactoidManager {
    @Override
    public void parse(String line, IMessageTarget target, User sender) {
        String name = line.split(" ")[0];
        String argsstring;
        try {
            argsstring = line.substring(name.length() + 1);
        } catch (Exception ex) {
            argsstring = "";
        }
        List<Match> args = Utils.getAllMatches("(\"(.*[^\\\\]|)\"|[^ \"][^ ]*)", argsstring);
        List<String> args_ = new LinkedList<String>();
        for (Match arg : args) {
            args_.add(arg.getMatch());
        }
        parse(name, args_, target, sender);
    }

    @Override
    public void parse(String name, List<String> args, IMessageTarget target, User sender) {
        parse(name, args, target, sender, new LinkedList<String>());
    }

    @Override
    public boolean exists(String name_) {
        return MiscCommands.factoids.containsKey(name_);
    }


    private void parse(String name, List<String> args, IMessageTarget target, User sender, List<String> resolveStack) {

        name = name.toLowerCase();

        if (!MiscCommands.factoids.containsKey(name))
            return;

        ConfigMap factoid = MiscCommands.factoids.getMap(name);
        String ip = factoid.getString("interpreter");
        IInterpreter ipr = null;
        if (ip != null) {
            ipr = InterpreterRegestry.resolve(ip);
        }
        String code = factoid.getString("data");
        if (ipr == null) {
            ipr = new PlaintextInterpreter();

        }// else {
        String type = factoid.getString("type", "regular");
        if (type.equalsIgnoreCase("regular")) {
            ipr.run(code, args, target, new UserSender(sender));
        } else if (type.equalsIgnoreCase("action")) {
            ipr.run(code, args, new ActionTarget(target), new UserSender(sender));
        } else if (type.equalsIgnoreCase("alias")) {
            String name_ = code.split(" ")[0].toLowerCase();
            if (!exists(name_)) {
                sender.getBot().sendNotice(sender, "As alias referenced factoid " + name_ + " does not exist.");
                return;
            }
            if (resolveStack.contains(name_.toLowerCase())) {
                sender.getBot().sendNotice(sender, "Cyclic alias on " + name_);
                return;
            }

            String argsstring;
            try {
                argsstring = code.substring(name.length());
            } catch (Exception ex) {
                argsstring = "";
            }
            List<Match> args1 = Utils.getAllMatches("(\"(.*[^\\\\]|)\"|[^ \"][^ ]*)", argsstring);
            List<String> args1_ = new LinkedList<String>();
            for (Match arg : args1) {
                args1_.add(arg.getMatch());
            }
            args1_.addAll(args);
            resolveStack.add(name_);
            parse(name_, args1_, target, sender, resolveStack);
        } else {
            ipr.run(code, args, target, new UserSender(sender));
            factoid.put("type", "regular");
            MiscCommands.factoids.save();
        }

        //}
    }
}
