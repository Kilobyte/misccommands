package de.kilobyte22.plugin.kibi.misccmds;

import de.kilobyte22.app.kibibyte.misc.ICommandSender;
import de.kilobyte22.app.kibibyte.misc.IInterpreter;
import de.kilobyte22.app.kibibyte.misc.IMessageTarget;
import de.kilobyte22.plugin.kibi.misccmds.vars.*;

import java.util.List;

public class PlaintextInterpreter implements IInterpreter {



    @Override
    public void run(String code, List<String> args, IMessageTarget target, ICommandSender sender) {
        try {
            String code2 = "";
            String token = null;

            TableValue env = new TableValue();
            env.set("nick", new StringValue(sender.getName()));
            env.set("channel", new StringValue(target.getName()));
            env.set("properties", new PropertyValue(target.getName()));

            for (int i = 0; i < args.size(); i++) {
                String arg = args.get(i);
                if (arg.equals("\"\""))
                    arg = "";
                if (arg.startsWith("\"") && arg.endsWith("\""))
                    arg = arg.substring(1, arg.length() - 1);
                arg = arg.replace("\\\"", "\"");
                args.set(i, arg);
            }
            env.set("args", new ArgsValue(args));

            boolean escaped = false;
            for(int i = 0; i < code.length(); i++) {
                char c = code.charAt(i);
                if (escaped) {
                    switch (c) {
                        default:
                            token += c;
                    }
                    escaped = false;
                } else {
                    switch (c) {
                        case '\\':
                            escaped = true;
                            break;
                        case '$':
                            if (token == null) token = "$";
                            break;
                        case '{':
                            if (token == null) {
                                code2 += "{";
                                break;
                            }
                            if (token.equals("$"))
                                token = "";
                            break;
                        case '}':
                            if (token == null) {
                                code2 += "}";
                                break;
                            }

                            code2 += parseToken(token, env, 0, TokenType.NONE);
                            token = null;
                            break;
                        default:
                            if (token != null && token.equals("$")) {
                                token = null;
                                code2 += "$" + c;
                            } else if (token != null) {
                                token += c;
                            } else {
                                code2 += c;
                            }
                    }
                }
            }
            target.print(code2);
        } catch (Exception ex) {
            sender.sendMessage("Error: " + ex.getClass().getName() + ": " + ex.getMessage());
        }
    }

    private Value parseToken(String token, TableValue env, int start, TokenType type) {
        Value val = env;
        TokenType tokenType = TokenType.STR_IDX;
        String tokenName = "";
        for (int i = 0; i < token.length(); i++) {
            char c = token.charAt(i);
            TokenType next = tokenType;
            boolean finishOff = false;
            switch (c) {
                case '#':
                    finishOff = true;
                    next = TokenType.NUM_IXD;
                    break;
                case ':':
                    finishOff = true;
                    next = TokenType.STR_IDX;
                    break;
                case '|':
                    finishOff = true;
                    next = TokenType.DEFAULT;
                    break;
                default:
                    tokenName += c;
            }
            if (finishOff) {
                switch (tokenType) {
                    case STR_IDX:
                        val = val.get(tokenName);
                        break;
                    case NUM_IXD:
                        val = val.get(Integer.parseInt(tokenName));
                        break;
                    case DEFAULT:
                        if (val == null || val == Value.NULL)
                            val = new StringValue(tokenName);
                        break;
                }
                tokenName = "";
                tokenType = next;
            }
        }
        switch (tokenType) {
            case STR_IDX:
                val = val.get(tokenName);
                break;
            case NUM_IXD:
                val = val.get(Integer.parseInt(tokenName));
                break;
            case DEFAULT:
                if (val == null || val == Value.NULL)
                    val = new StringValue(tokenName);
                break;
        }
        return val;
    }

    private enum TokenType {
        NONE,
        NUM_IXD,
        STR_IDX,
        DEFAULT
    }
}
