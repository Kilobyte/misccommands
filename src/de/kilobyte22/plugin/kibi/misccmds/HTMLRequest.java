package de.kilobyte22.plugin.kibi.misccmds;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

public class HTMLRequest {

    private final URL url;
    private HashMap<String, String> headers = new HashMap<String, String>();
    private String postData;

    public HTMLRequest(URL url) {

        this.url = url;
    }

    public HTMLRequest addHeader(String name, String value) {
        headers.put(name, value);
        return this;
    }

    public HTMLRequest setPostData(String s) {
        postData = s;
        return this;
    }

    public HTMLResponse send() {
        URLConnection c = null;
        try {
            c = url.openConnection();
            for (String k : headers.keySet()) {
                c.addRequestProperty(k, headers.get(k));
            }
            if (postData != null) {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(c.getOutputStream()));
                out.write(postData);
                out.flush();
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));

            String inputLine;
            String resBody = "";

            while ((inputLine = in.readLine()) != null) {
                if (!resBody.equals("")) resBody += "\n";
                resBody += inputLine;
            }

            return new HTMLResponse(resBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
