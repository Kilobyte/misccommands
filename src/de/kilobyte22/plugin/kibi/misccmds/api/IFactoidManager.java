package de.kilobyte22.plugin.kibi.misccmds.api;

import de.kilobyte22.app.kibibyte.misc.IMessageTarget;
import org.pircbotx.User;

import java.util.List;

public interface IFactoidManager {
    //public static IFactoidManager MANAGER = null;

    void parse(String line, IMessageTarget target, User sender);
    void parse(String name, List<String> args, IMessageTarget target, User sender);
    boolean exists(String name_);
}
