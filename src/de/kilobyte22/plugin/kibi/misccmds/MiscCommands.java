package de.kilobyte22.plugin.kibi.misccmds;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;
import de.kilobyte22.app.kibibyte.command.CommandHandler;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.event.BotSavingEvent;
import de.kilobyte22.app.kibibyte.event.RehashEvent;
import de.kilobyte22.app.kibibyte.misc.TimeUtils;
import de.kilobyte22.app.kibibyte.misc.Utils;
import de.kilobyte22.app.kibibyte.plugin.BotAccess;
import de.kilobyte22.lib.configuration.ConfigFile;
import de.kilobyte22.lib.configuration.ConfigList;
import de.kilobyte22.lib.configuration.ConfigMap;
import de.kilobyte22.lib.configuration.ConfigNode;
import de.kilobyte22.app.kibibyte.misc.Match;
import de.kilobyte22.lib.logging.LogLevel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.hooks.events.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class MiscCommands implements IPlugin {

    public static BotAccess botAccess;
    public static ConfigFile config, tellStorage, factoids, seenData;
    public static int googlePmResults, googleChannelResults;
    public static String googleApiKey;

    public boolean joinOnInvite = false, autoJoinOnJoin = false, antiAutoJoinOnKick = true;
    public String factoidprefix;

    @Override
    public void load(BotAccess access) {
        {
            File miscFile = new File("misc");
            if (!miscFile.exists())
                miscFile.mkdir();
        }
        botAccess = access;
        botAccess.eventBus.register(this);
        config = botAccess.config;
        tellStorage = new ConfigFile(new File("misc/tellstorage.yml"));
        tellStorage.load();
        factoids = new ConfigFile(new File("misc/factoids.yml"));
        factoids.load();
        seenData = new ConfigFile(new File("misc/seen.yml"));
        seenData.load();
        botAccess.commandManager.registerHandler(Commands.class);
    }

    @Override
    public void unload() {
    }

    @Override
    public void enable() {
        onRehash(null);
        for (ConfigNode s : config.getList("channels")) {
            botAccess.bot.join(s.toString());
        }
    }

    @Override
    public void disable() {
    }

    @Subscribe
    private void onRehash(RehashEvent event) {
        config.load();
        ConfigMap google = config.getMap("google");
        googlePmResults = google.getInt("pmResultCount", 4);
        googleChannelResults = google.getInt("channelResultCount", 1);
        googleApiKey = google.getString("APIKey", "");
        if (googleApiKey.equals(""))
            googleApiKey = null;

        joinOnInvite = config.getBoolean("joinOnInvite", false);
        autoJoinOnJoin = config.getBoolean("autoJoinOnJoin", false);
        antiAutoJoinOnKick = config.getBoolean("antiAutoJoinOnKick", true);

        factoidprefix = config.getString("factoidpref", "?!");
        config.save();
        tellStorage.load();
    }

    @Subscribe
    public void onNick(NickChangeEvent event) {
        doTells(event.getUser());
    }

    @Subscribe
    public void onInvite(InviteEvent event) {
        if (!joinOnInvite) return;
        if (config.getList("channelblacklist").containsObject(event.getChannel().toLowerCase())) {
            botAccess.logger.log(LogLevel.INFO, String.format("%s invited me to %s, but it is blacklisted", event.getUser(), event.getChannel()));
            return;
        }
        botAccess.bot.join(event.getChannel());
        botAccess.logger.log(LogLevel.INFO, String.format("%s invited me to %s, Joining it", event.getUser(), event.getChannel()));
    }

    @Subscribe
    public void onJoin(JoinEvent event) {
        if (autoJoinOnJoin && event.getUser() == botAccess.kibibyte.getUserBot()) {
            botAccess.kibibyte.channels.add(event.getChannel().getName());
            if (!Kibibyte.config.getMap("misc").getList("channels").containsObject(event.getChannel().getName())) {
                Kibibyte.config.getMap("misc").getList("channels").add(event.getChannel().getName());
                Kibibyte.config.save();
            }
            /*ConfigList clist = config.getList("channels");
            if (!clist.containsObject(event.getChannel().getName())) {
                clist.add(event.getChannel().getName());
                config.save();
            }*/
        }
        if (event.getUser() != botAccess.kibibyte.getUserBot()) {
            doTells(event.getUser());
        }

    }

    private void doTells(User user) {
        if (tellStorage.getMap("messages").containsKey(user.getNick().toLowerCase())) {
            for (ConfigNode n : tellStorage.getMap("messages").getList(user.getNick().toLowerCase())) {
                ConfigMap m = (ConfigMap) n;
                botAccess.bot.sendNotice(user, m.getString("sender") + " left a message for you: " + m.getString("message"));
            }
            tellStorage.getMap("messages").remove(user.getNick().toLowerCase());
        }
        tellStorage.save();
    }

    @Subscribe
    public void onMessage(MessageEvent event) {
        doTells(event.getUser());

        ConfigMap seen = seenData.getMap(event.getUser().getNick().toLowerCase());
        seen.put("channel", event.getChannel().getName());
        seen.put("timestamp", TimeUtils.getCurrentTimeStamp());
        seen.put("message", event.getMessage());

        if (!botAccess.permissionSystem.mayUse(event.getUser(), event.getChannel())) return;

        if (factoidprefix.contains(event.getMessage().substring(0, 1)) && botAccess.permissionSystem.hasParsedPermission("factoid.display", event.getUser(), event.getChannel())) {
            String fact = event.getMessage().substring(1);
            Factoids.parse(fact, new ChannelTarget(event.getChannel()), event.getUser());
        }
        if (googleApiKey != null) {
            List<Match> matches = Utils.getAllMatches("(https?://)?(www\\.)?youtu(\\.be/|be\\.[a-z]{2,4}/watch/?\\?v=)([a-zA-Z0-9_-]+)", event.getMessage());
            if (matches.size() > 0 && botAccess.permissionSystem.hasParsedPermission("misc.youtubepreview", event.getUser(), event.getChannel()) || botAccess.kibibyte.propertySystem.getProperty("youtubeeveryone", event.getChannel().getName()) != null)
                for (Match m : matches) {
                    //botAccess.logger.log("Found youtube link: " + m.getMatch());
                    youtubeLookup(event.getChannel(), event.getUser(), m.getGroups()[3]);
                }
        }
    }

    private void youtubeLookup(Channel channel, User user, String code) {
        try {
            URL url = new URL("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + URLEncoder.encode(code) + "&key=" + googleApiKey);// + "&userIp=" + user.getHostmask());
            JSONObject obj = new JSONObject(HTTP.get(url));
            JSONObject pageInfo = obj.getJSONObject("pageInfo");
            int results = pageInfo.getInt("totalResults");
            if (results < 1)
                return;
            JSONArray items = obj.getJSONArray("items");
            JSONObject video = items.getJSONObject(0);
            JSONObject snippet = video.getJSONObject("snippet");
            String title = snippet.getString("title"), vidchannel = snippet.getString("channelTitle");
            botAccess.bot.sendMessage(channel, String.format("[%s] " + CommandHandler.BOLD + " %s " + CommandHandler.RESET + "by " + CommandHandler.BOLD + "%s", user.getNick(), title, vidchannel));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onKick(KickEvent event) {
        if (antiAutoJoinOnKick && event.getRecipient() == botAccess.kibibyte.getUserBot()) {
            botAccess.kibibyte.channels.remove(event.getChannel().getName());
            Kibibyte.config.getMap("misc").getList("channels").removeObject(event.getChannel().getName());
            Kibibyte.config.save();
        }
    }

    @Subscribe
    public void onBotSave(BotSavingEvent event) {
        seenData.save();
    }
}
