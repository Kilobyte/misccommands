package de.kilobyte22.plugin.kibi.misccmds;

import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.misc.ICommandSender;
import org.pircbotx.User;

public class UserSender implements ICommandSender {
    private final User sender;

    public UserSender(User sender) {

        this.sender = sender;
    }

    @Override
    public String getName() {
        return sender.getNick();
    }

    @Override
    public String sendMessage(String message) {
        MiscCommands.botAccess.bot.sendNotice(sender, message);
        return null;
    }
}
