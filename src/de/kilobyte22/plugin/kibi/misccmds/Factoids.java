package de.kilobyte22.plugin.kibi.misccmds;

import de.kilobyte22.app.kibibyte.api.InterpreterRegestry;
import de.kilobyte22.app.kibibyte.misc.Args;
import de.kilobyte22.app.kibibyte.misc.IInterpreter;
import de.kilobyte22.app.kibibyte.misc.IMessageTarget;
import de.kilobyte22.app.kibibyte.misc.Utils;
import de.kilobyte22.lib.configuration.ConfigMap;
import de.kilobyte22.plugin.kibi.misccmds.api.IFactoidManager;
import org.pircbotx.User;

import java.util.HashMap;
import java.util.List;

public class Factoids {

    private static IFactoidManager mgr = new FactoidManger();

    public static void parse(String line, IMessageTarget target, User sender) {
        mgr.parse(line, target, sender);
    }

    public static void parse(String name, List<String> args, IMessageTarget target, User sender) {
        mgr.parse(name, args, target, sender);
    }

    public static boolean exists(String name_) {
        return mgr.exists(name_);
    }
}
