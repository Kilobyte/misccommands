package de.kilobyte22.plugin.kibi.misccmds.vars;

public class StringValue extends Value {

    private String data;

    public StringValue(String val) {
        data = val;
    }

    @Override
    public String getType() {
        return "string";
    }

    @Override
    public String toString() {
        return data;
    }
}
