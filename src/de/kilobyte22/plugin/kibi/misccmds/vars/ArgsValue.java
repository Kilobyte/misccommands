package de.kilobyte22.plugin.kibi.misccmds.vars;

import de.kilobyte22.app.kibibyte.misc.Args;

import java.util.List;

public class ArgsValue extends Value {

    private final List<String> args;

    public ArgsValue(List<String> args) {

        this.args = args;
    }

    @Override
    public Value get(String key) {
        String[] parts = key.split(";");
        if (parts.length < 2)
            parts = new String[]{parts[0], ""};
        int start, stop;
        if (parts[0].equals(""))
            start = 0;
        else
            start = Integer.parseInt(parts[0]);

        if (parts[1].equals(""))
            stop = args.size();
        else
            stop = Integer.parseInt(parts[1]) + 1;

        String ret = "";
        for (int i = start; i < stop; i++) {
            if (i != start) ret += " ";
            ret += args.get(i);
        }
        if (ret.trim().length() > 0)
            return new StringValue(ret);
        return NULL;
    }

    @Override
    public Value get(int key) {
        try {
            return new StringValue(args.get(key));
        } catch (IndexOutOfBoundsException ex) {
            return NULL;
        }
    }

    @Override
    public String getType() {
        return "args";
    }
}
