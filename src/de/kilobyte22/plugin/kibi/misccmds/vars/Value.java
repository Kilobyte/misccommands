package de.kilobyte22.plugin.kibi.misccmds.vars;

public abstract class Value {

    public static final Value NULL = new NullValue();

    public Value get(String idx) {
        throw new VarError("Attempt to index + " + getType());
    }

    public void set(String idx, Value val) {
        throw new VarError("Attempt to index + " + getType());
    }

    public Value get(int idx) {
        throw new VarError("Attempt to index + " + getType());
    }

    public void set(int idx, Value val) {
        throw new VarError("Attempt to index + " + getType());
    }

    public abstract String getType();
}
