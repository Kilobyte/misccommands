package de.kilobyte22.plugin.kibi.misccmds.vars;

import java.util.HashMap;

public class TableValue extends Value {

    private HashMap<String, Value> stringvals = new HashMap<String, Value>();
    private HashMap<Integer, Value> intvals = new HashMap<Integer, Value>();

    public Value get(String idx) {
        Value ret = stringvals.get(idx);
        return ret == null ? NULL : ret;
    }

    public void set(String idx, Value val) {
        stringvals.put(idx, val);
    }

    public Value get(int idx) {
        Value ret = intvals.get(idx);
        return ret == null ? NULL : ret;
    }

    public void set(int idx, Value val) {
        intvals.put(idx, val);
    }

    @Override
    public String getType() {
        return "table";
    }

    @Override
    public String toString() {
        return "table";
    }
}
