package de.kilobyte22.plugin.kibi.misccmds.vars;

public class NullValue extends Value {
    @Override
    public String getType() {
        return "null";
    }

    @Override
    public String toString() {
        return "null";
    }
}
