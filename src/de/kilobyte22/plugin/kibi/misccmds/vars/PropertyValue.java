package de.kilobyte22.plugin.kibi.misccmds.vars;

import de.kilobyte22.lib.configuration.ConfigNode;
import de.kilobyte22.plugin.kibi.misccmds.MiscCommands;

public class PropertyValue extends Value {
    private final String channel;

    @Override
    public Value get(String key) {
        ConfigNode n = MiscCommands.botAccess.kibibyte.propertySystem.getProperty(key, channel);
        if (n == null)
            return new NullValue();
        return new StringValue(n.toString());
    }

    public PropertyValue(String channel) {

        this.channel = channel;
    }

    @Override
    public String getType() {
        return "properties";
    }
}
