package de.kilobyte22.plugin.kibi.misccmds.vars;

public class VarError extends RuntimeException {
    public VarError(String s) {
        super(s);
    }
}
