package de.kilobyte22.plugin.kibi.misccmds;

import de.kilobyte22.app.kibibyte.misc.IMessageTarget;

public class ActionTarget implements IMessageTarget {
    private final IMessageTarget target;

    public ActionTarget(IMessageTarget target) {
        this.target = target;
    }

    @Override
    public void print(String message) {
        target.print("\u0001ACTION " + message + "\u0001");
    }

    @Override
    public String getName() {
        return target.getName();
    }
}
